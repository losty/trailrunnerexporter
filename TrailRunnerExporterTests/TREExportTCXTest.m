//
//  TREEExportTCX.m
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 19.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TREExportTCXTest.h"
#import "TREWaypoint.h"
#import "TREExportTCX.h"

@implementation TREExportTCXTest

// All code under test must be linked into the Unit Test bundle
- (void)testWayptoint
{
    
    TREWaypoint *wpt = [[TREWaypoint alloc] init];

    [wpt setDate:[NSDate dateWithString:@"2012-06-18 22:55:41 +0000"]];
    [wpt setLat:[NSNumber numberWithDouble:49.10]];
    [wpt setLon:[NSNumber numberWithDouble:12.5]];
    [wpt setAlt:[NSNumber numberWithDouble:100]];
    
    NSString *waypointXmlIS = [TREExportTCX serializeWaypoint:wpt];
    NSString *waypointXmlSHOULDBE = @"\t\t\t\t<Trackpoint>\n\t\t\t\t\t<Time>2012-06-18T22:55:41Z</Time>\n\t\t\t\t\t<Position>\n\t\t\t\t\t\t<LatitudeDegrees>49.1</LatitudeDegrees>\n\t\t\t\t\t\t<LongitudeDegrees>12.5</LongitudeDegrees>\n\t\t\t\t\t</Position>\n\t\t\t\t\t<AltitudeMeters>100</AltitudeMeters>\n\t\t\t\t\t<DistanceMeters>(null)</DistanceMeters>\n\t\t\t\t</Trackpoint>\n";
    
    STAssertEqualObjects(waypointXmlSHOULDBE, waypointXmlIS, @"Waypoint XML failed");
    
}

@end
