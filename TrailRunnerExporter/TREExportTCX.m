//
//  TREExportTCX.m
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 18.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TREExportTCX.h"
#import "TREActivity.h"

@implementation TREExportTCX

static NSArray *activityTypes = NULL;
static NSDateFormatter *df = NULL;

+(NSDateFormatter *)dateFormatter{
    if (df == NULL) {
        df = [[NSDateFormatter alloc] init];
        [df setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [df setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];
    }
    return df;
}


+(NSString *)serializeWaypoint:(TREWaypoint*)wpt {
    
    NSDateFormatter *df = [self dateFormatter];
    
    NSMutableString *xml = [NSMutableString stringWithFormat:@""];

    [xml appendString:@"\t\t\t\t<Trackpoint>\n"];
    [xml appendString:[NSString stringWithFormat:@"\t\t\t\t\t<Time>%@</Time>\n", [df stringFromDate:[wpt date]]]];
    [xml appendString:[NSString stringWithFormat:@"\t\t\t\t\t<Position>\n"]];
    [xml appendString:[NSString stringWithFormat:@"\t\t\t\t\t\t<LatitudeDegrees>%@</LatitudeDegrees>\n", [wpt lat]]];
    [xml appendString:[NSString stringWithFormat:@"\t\t\t\t\t\t<LongitudeDegrees>%@</LongitudeDegrees>\n", [wpt lon]]];
    [xml appendString:[NSString stringWithFormat:@"\t\t\t\t\t</Position>\n"]];
    [xml appendString:[NSString stringWithFormat:@"\t\t\t\t\t<AltitudeMeters>%@</AltitudeMeters>\n", [wpt alt]]];
    [xml appendString:[NSString stringWithFormat:@"\t\t\t\t\t<DistanceMeters>%@</DistanceMeters>\n", [wpt distance]]];
    if ([wpt heartRate]) {
        [xml appendString:[NSString stringWithFormat:@"\t\t\t\t\t<HeartRateBpm>\n\t\t\t\t\t\t<Value>%@</Value>\n\t\t\t\t\t</HeartRateBpm>\n", [wpt heartRate]]];
    }
    if ([wpt cadence]) {
        [xml appendString:[NSString stringWithFormat:@"\t\t\t\t\t<Cadence>%@</Cadence>\n", [wpt cadence]]];
    }
    [xml appendString:@"\t\t\t\t</Trackpoint>\n"];
    
    return xml;    

}

+(NSString *)serializeLap:(TRELap*)lap {
    
    NSDateFormatter *df = [self dateFormatter];
    
    NSMutableString *xml = [NSMutableString stringWithFormat:@""];
    if ([lap duration])
        [xml appendString:[NSString stringWithFormat:@"\t\t\t<TotalTimeSeconds>%@</TotalTimeSeconds>\n", [lap duration]]];
    if ([lap distance])
        [xml appendString:[NSString stringWithFormat:@"\t\t\t<DistanceMeters>%@</DistanceMeters>\n", [lap distance]]];
    if ([lap maxSpeed])
        [xml appendString:[NSString stringWithFormat:@"\t\t\t<MaximumSpeed>%@</MaximumSpeed>\n", [lap maxSpeed]]];
    if ([lap calories])
        [xml appendString:[NSString stringWithFormat:@"\t\t\t<Calories>%@</Calories>\n", [lap calories]]];
    if ([lap avgHeartRate])
        [xml appendString:[NSString stringWithFormat:@"\t\t\t<AverageHeartRateBpm>\n\t\t\t\t<Value>%@</Value>\n\t\t\t</AverageHeartRateBpm>\n", [lap avgHeartRate]]];
    if ([lap maxHeartRate])
        [xml appendString:[NSString stringWithFormat:@"\t\t\t<MaximumHeartRateBpm>\n\t\t\t\t<Value>%@</Value>\n\t\t\t</MaximumHeartRateBpm>\n", [lap maxHeartRate]]];
    if ([lap avgCadence])
        [xml appendString:[NSString stringWithFormat:@"\t\t\t<Cadence>%@</Cadence>\n", [lap avgCadence]]];
    
    [xml appendString:@"\t\t\t<Intensity>Active</Intensity>\n"];
    [xml appendString:@"\t\t\t<TriggerMethod>Manual</TriggerMethod>\n"];
    
    [xml appendString:@"\t\t\t<Track>\n"];
    for (TREWaypoint *wpt in [lap track]) {
        [xml appendString:[self serializeWaypoint:wpt]];
    }
    [xml appendString:@"\t\t\t</Track>\n"];
    
    if([lap avgSpeed]) {
        [xml appendString:[NSString stringWithFormat:@"\t\t\t<Extensions>\n\t\t\t\t<LX xmlns=\"http://www.garmin.com/xmlschemas/ActivityExtension/v2\">\n\t\t\t\t\t<AvgSpeed>%@</AvgSpeed>\n\t\t\t\t</LX>\n\t\t\t</Extensions>\n", [lap avgSpeed]]];
    }

    return [NSString stringWithFormat:@"\t\t<Lap StartTime=\"%@\">\n%@\t\t</Lap>\n", [df stringFromDate:[[[lap track] objectAtIndex:0] date]], xml];
         
}
         
+(NSString *)serializeActivity:(TREActivity*)activity {

    if (activityTypes == NULL) {
        activityTypes = [NSArray arrayWithObjects:
                         @"Other" /*Sonstiges*/, 
                         @"Running" /*Walking*/, 
                         @"Running" /*Laufen*/, 
                         @"Biking" /*Radfahren*/, 
                         @"Biking" /*Rennrad*/, 
                         @"Biking" /*Mountainbike*/, 
                         @"Other" /*Skaten*/,
                         @"Other" /*Schwimmen*/,
                         @"Other" /*Ski*/,
                         @"Other" /*Tourenski*/,
                         @"Other" /*Motorisiert*/,
                         nil];
    }
    NSDateFormatter *df = [self dateFormatter];
    
    NSMutableString *xml = [NSMutableString stringWithFormat:@""];
    [xml appendString:[NSString stringWithFormat:@"\t\t<Activity Sport=\"%@\">\n", [activityTypes objectAtIndex:[[activity type] intValue]]]];
    [xml appendString:[NSString stringWithFormat:@"\t\t<Id>%@</Id>\n", [df stringFromDate:[activity date]]]];
    
    for (TRELap *lap in [activity laps]) {
        [xml appendString:[self serializeLap:lap]];
    }
    
    [xml appendString:@"\t\t</Activity>\n"];
    
    return xml;

}

@end
