//
//  TRELap.h
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 18.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TREWaypoint.h"

@interface TRELap : NSObject {
    NSNumber *duration;
    NSNumber *distance;
    NSNumber *avgSpeed;
    NSNumber *maxSpeed;
    NSNumber *calories;
    NSNumber *avgHeartRate;
    NSNumber *maxHeartRate;
    NSNumber *avgCadence;
    NSArray *track;
}

@property (retain) NSNumber* duration;
@property (retain) NSNumber* distance;
@property (retain) NSNumber* avgSpeed;
@property (retain) NSNumber* maxSpeed;
@property (retain) NSNumber* calories;
@property (retain) NSNumber* avgHeartRate;
@property (retain) NSNumber* maxHeartRate;
@property (retain) NSNumber* avgCadence;
@property (retain) NSArray* track;

@end
