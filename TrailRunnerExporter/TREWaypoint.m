//
//  TREWaypoint.m
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 18.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TREWaypoint.h"

@implementation TREWaypoint

@synthesize date;
@synthesize lat;
@synthesize lon;
@synthesize alt;
@synthesize distance;
@synthesize heartRate;
@synthesize cadence;

@end
