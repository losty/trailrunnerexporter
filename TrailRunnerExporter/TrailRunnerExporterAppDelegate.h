//
//  TrailRunnerExporterAppDelegate.h
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 17.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface TrailRunnerExporterAppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;
@property (assign) IBOutlet NSTextField *inFileName;
@property (assign) IBOutlet NSTextField *outFileName;
@property (assign) IBOutlet NSButton *goButton;
@property (assign) IBOutlet NSProgressIndicator *processBar;

- (IBAction)exportActivities:(id)sender;
- (IBAction)chooseInFile:(id)sender;
- (IBAction)chooseOutDir:(id)sender;

- (void)alert:(NSString*)msg;

@end
